--
-- Premake script (http://premake.github.io)
--

solution 'lua-msgpack'
    configurations  {'Debug', 'Release'}
    targetdir       'bin'

    filter 'configurations:Debug'
        defines     { 'DEBUG' }
        flags       { 'Symbols' }

    filter 'configurations:Release'
        defines     { 'NDEBUG' }
        flags       { 'Symbols'}
        optimize    'On'

    filter 'action:vs*'
        defines
        {
            'WIN32',
            'WIN32_LEAN_AND_MEAN',
            '_WIN32_WINNT=0x0600',
            '_CRT_SECURE_NO_WARNINGS',
            'NOMINMAX',
            'inline=__inline',
			'LUA_BUILD_AS_DLL',
        }

    filter 'action:gmake'
        buildoptions    '-std=c99'

	project 'lua'
        targetname  'lua'
		language    'C'
		kind        'ConsoleApp'
		location    'build'

        includedirs 'dep/lua/src'
		files       'dep/lua/src/lua.c'
		libdirs     'bin'
		links       'lua5.3'

        filter 'system:linux'
            defines 'LUA_USE_LINUX'
            links { 'm', 'dl', 'readline'}

	project 'lua5.3'
        targetname  'lua5.3'
		language    'C'
		kind        'SharedLib'
		location    'build'

        includedirs 'dep/lua/src'
		files
		{
			'dep/lua/src/*.h',
			'dep/lua/src/*.c',
		}
		removefiles
		{
			'dep/lua/src/lua.c',
			'dep/lua/src/luac.c',
		}

        filter 'system:linux'
            defines 'LUA_USE_LINUX'

	project 'libmsgpack'
        targetname  'libmsgpack'
		language    'C'
		kind        'StaticLib'
		location    'build'
        includedirs 'dep/msgpack/include'
        files
        {
            'dep/msgpack/include/*.h',
            'dep/msgpack/src/*.c',
        }

    project 'luamsgpack'
        targetname  'luamsgpack'
        language    'C'
        kind        'SharedLib'
        location    'build'
		defines     'LUA_LIB'
        includedirs 'dep/msgpack/include'
        files
        {
            'src/*.h',
            'src/*.c',
        }
        includedirs
        {
            'dep/libmysql',
            'dep/lua/src',
        }
        libdirs 'bin'
        
        filter 'action:vs*'
            links   {'lua5.3', 'libmsgpack'}
