# lua-msgpack

Simple Lua 5.3 binding of [msgpack-c](https://github.com/msgpack/msgpack-c)

## Installation

First obtain [premake5](http://premake.github.io/download.html)

### Build on Windows 7 x64

1. Type `premake5 vs2013` on command window to generate Visual C++ solution files.
2. Use Visual Studio 2013(either Ultimate or Community version) to compile executable binary.
3. Pre-compiled Windows x86/64 libmysql binary can be obtained [here](https://github.com/ichenq/usr)

### Build on Ubuntu 14.04 64-bit

1. Type `sudo apt-get install libmysqlclient-dev`
2. Type `premake5 gmake && make config=release`


## Example usage

~~~~~~~~~~lua
local mp = require 'msgpack'

local t = 
{
    author = 'ichenq',
    url = 'https://github.com/ichenq/lua-msgpack'
    star = 1,
    license = 'MIT',
}
local data = mp.pack(t)
local t2 = mp.unpack(data)
~~~~~~~~~~

## Issues

* msgpack `ext` type are not supported.